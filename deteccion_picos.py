import numpy as np
from scipy import signal as sg
from matplotlib import pyplot as plt


x = np.linspace(0,10,1000)
y = np.sin(2*np.pi*3*x)
indices,_ = sg.find_peaks(y,max(y)/10)

ruido = 0.1*np.random.randn(len(x))
y = y+ruido

y_fft = abs(np.fft.rfft(y))
frecs = np.fft.rfftfreq(len(y),x[1]-x[0])

frec_osc,_ = sg.find_peaks(y_fft,max(y_fft)/10)

fig,[ax,ax_fft] = plt.subplots(2)
ax.plot(x,y)
ax.plot(x[indices],y[indices],'o')
ax_fft.plot(frecs,abs(y_fft))
ax_fft.plot(frecs[frec_osc],abs(y_fft[frec_osc]),'o')
plt.show()


