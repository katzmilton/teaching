import numpy as np
import matplotlib.pyplot as plt

def grafico_intensidad(N_rendijas,ancho_rendijas,ancho_hueco,sen_tita,sen_tita0):

    gamma = np.pi*(ancho_rendijas + ancho_hueco)*(sen_tita-sen_tita0) #ancho_rendijas y ancho_hueco están en unidades de lambda

    beta = np.pi*ancho_rendijas*(sen_tita-sen_tita0)
    interferencia = []
    campana_difraccion = []
    for i in range(len(beta)):
        if beta[i] == 0:
            campana_difraccion.append(1)
        else:
            campana_difraccion.append((np.sin(beta[i])/beta[i])**2)
        if gamma[i] == 0:
            interferencia.append(0)
        else:
            interferencia.append((np.sin(N_rendijas*gamma[i])/np.sin(gamma[i]))**2)
    interferencia = np.array(interferencia)
    campana_difraccion = np.array(campana_difraccion)
    fig,ax = plt.subplots()
    ax.plot(sen_tita,interferencia*campana_difraccion/max(interferencia),'grey')
    ax.plot(sen_tita,campana_difraccion,ls = '--',color = 'grey')
    ax.set_xlabel(r'sen ($\theta$)')
    ax.set_ylabel('Intensidad [u.a]')

    return (fig,ax)

sen_tita = np.linspace(-1,1,1000)
tema2_fig,tema2_ax= grafico_intensidad(6,3,1,sen_tita,-0.5)

tema2_fig.savefig('tema2.png')
