import numpy as np
import matplotlib.pyplot as plt

filename = 'tabla_ojocompuesto'
parametros = np.loadtxt(filename) #rad [mm], err_rad [mm], b [um], err_b [um]

lambdas = np.linspace(300,650,100) #nm
cmap = plt.get_cmap('jet')

radios_teoricos = np.linspace(0,4,500)

fig,ax = plt.subplots()
for lam_i in range(1,len(lambdas)):
    ax.fill_between(radios_teoricos,np.sqrt(lambdas[lam_i-1]*radios_teoricos),np.sqrt(lambdas[lam_i]*radios_teoricos),alpha = 0.15,color = cmap(lam_i/len(lambdas)))
ax.errorbar(parametros[:,0],parametros[:,2],xerr = parametros[:,1],yerr = parametros[:,3],ls = '',marker = 'o')
ax.set_xlabel('Profundidad del ojo (radio) [mm]')
ax.set_ylabel('Apertura de la ommatidia (b) [$\mu$m]')
# ax.grid()
fig.savefig('apertura_ommatidia.png')
plt.show()
