import matplotlib.pyplot as plt
import numpy as np

m = 3
wm = 2*np.pi
wp = np.pi

t = np.linspace(0,4,100)
X = (m+1)*np.cos(wm*t)*np.cos(wp*t)+(m-1)*np.sin(wm*t)*np.sin(wp*t)

fig,ax = plt.subplots()
ax.grid()
ax.plot(t,X)
ax.set_xlabel('Tiempo')
ax.set_ylabel('$\psi$')

fig.savefig('Batido')
plt.plot()
