import numpy as np
import matplotlib.pyplot as plt

A0 = 0.1
omega = np.linspace(7,np.sqrt(95),100)
kappa = np.sqrt(9.8/0.1 - omega**2)
k = np.sqrt(omega**2 - 9.8/0.2)

alpha = np.arctan(k/kappa)
z = np.linspace(0,1,1000)

A_z = A0*np.exp(-kappa[30]*z)/(np.sqrt(1+(kappa[30]/k[30])**2)*np.cos(alpha[30]))
A_omega = A0*np.exp(-kappa/kappa[30])/(np.sqrt(1+(kappa/k)**2)*np.cos(alpha))

fig_long,ax_long = plt.subplots()
ax_long.plot(z,A_z,color = '#81a1c1')
ax_long.plot([1/kappa[30]]*2,[0,max(A_z)],ls='--',color = '#a3be8c')
ax_long.set_xlabel('distacia (m)')
ax_long.set_ylabel('Amplitud')

fig_transf, ax_transf = plt.subplots()
ax_transf.plot(omega,A_omega, color = '#81a1c1')
ax_transf.plot([np.sqrt(9.8/0.1 - kappa[30]**2)]*2,[0,max(A_omega)],ls='--',color = '#a3be8c')
ax_transf.set_xlabel('$\Omega$ (1/s)')
ax_transf.set_ylabel('Amplitud')

fig_kappa, ax_kappa = plt.subplots()
ax_kappa.plot(omega,kappa,color = '#81a1c1')
ax_kappa.set_xlabel('$\Omega$ (1/s)')
ax_kappa.set_ylabel('$\kappa$ (1/m)')

fig_long.savefig('A_z.png')
fig_kappa.savefig('kappa.png')
fig_transf.savefig('A_omega.png')
plt.show()
