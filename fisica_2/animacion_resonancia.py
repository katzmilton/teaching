# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 11:57:00 2020

@author: Milton Katz
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
import math

from matplotlib import animation, rc

#%%

#Creo el subplot donde voy a graficar
fig, (axX,axM,axA) = plt.subplots(1,3)

fig.set_size_inches((10,3)) #Seteo el tamaño de la imagen que voy a exportar, dentro de python no sirve para nada pero si después lo guardo si
fig.tight_layout(pad=3.0) #Si tengo varios graficos en una misma figura esto me los separa

#Un poquito de maquillaje para los gráficos
axX.set_xlim((0,10)) 
axX.set_ylim((-1.5,1.5))
axX.grid()
axX.set_xlabel('Tiempo')
axX.set_ylabel('Posición')

axM.set_xlim((-2,2))
axM.set_ylim((-2,2))
axM.set_xlabel('Posición X')
axM.set_ylabel('Posición Y')

axA.set_xlim((0,5))
axA.set_ylim((-.5,2))
axA.set_xlabel('Frecuencia ($\Omega$)')
axA.set_ylabel('Amplitud')
axA.grid()

#frames es el número de frames que va a tener mi animación, cuanto más alto más se tarda en generarlo, en este código no me estoy preocupando mucho por el tiempo pero para ir probando cosas está bueno bajarlo
frames = 200

#Creo las instancias de que voy a graficar, por ahora vacías, se van a llenar después
lineX, = axX.plot([], [], lw=2)
lineM, = axM.plot([], [], 'o',ms = 15,zorder = 3)
lineRes, = axM.plot([],[],ls = '--',color = 'k', lw = 2, zorder = 2)
lineA, = axA.plot([], [], 'o', ms=10,zorder = 3)

#El método FuncAnimation me va a pedir dos funciones, una para inicializar los plots y otra para animarlos, puedo poner la cantidad de plots que quiera, agregar o sacar
def init():
    lineX.set_data([],[])
    lineM.set_data([],[])
    lineRes.set_data([],[])
    lineA.set_data([],[])
    return (lineX,lineM,lineRes,lineA,)

#FuncAnimation va a iterar en un rango de 0 a frame, pasandole i a animate, yo hago de cuenta que i se comporta como en un for aunque ese for nunca lo escriba.
def animate(i):
    #Defino las constantes útiles
    m=1
    F = 1
    omega0 = 2
    t = np.linspace(0,10,1000) #t lo uso cuando es la variable indepte de una función, tv la uso cuando es un parámetro y lo que animo es un punto
    tv = np.linspace(0,10,frames) #t tiene un tamaño arbitrario porque cada paso de la animación tiene una curva de tamaño t, tv es un parámetro que cambia cada frame al igual que omega y tiene que haber tantos tv como frames
    omega = np.linspace(0.5,3.5,frames)
    gamma = 0.5
    
    #Creo las matrices de mis funciones, cada fila es un frame, me sirven para animar funciones
    X = np.zeros((frames,len(t)))
    A = np.zeros((frames,len(t)))
    B = np.zeros((frames,len(t)))
    
    #Estos son para animar puntos, en cada frame tienen un solo valor, por eso son un vector y no una matriz
    Xv = np.zeros(frames) 
    Av = np.zeros(frames)
    Bv = np.zeros(frames)
    
    #Defino mis funciones explícitamente para cada frame i y se lo asigno al plot
    A[i] += (F/m)*gamma*omega[i]/(gamma**2 * omega[i]**2 + (omega0**2 - omega[i]**2)**2)    
    B[i] += (F/m)*(omega0**2 - omega[i]**2)/(gamma**2 * omega[i]**2 + (omega0**2 - omega[i]**2)**2)
    X[i] += A[i]*np.sin(omega[i]*t) + B[i]*np.cos(omega[i]*t)
    lineX.set_data(t,X[i])
    
    #Defino las coordenadas de mis puntos para cada variable y se los asigno a los plots
    Av[i] += (F/m)*gamma*omega[i]/(gamma**2 * omega[i]**2 + (omega0**2 - omega[i]**2)**2)    
    Bv[i] += (F/m)*(omega0**2 - omega[i]**2)/(gamma**2 * omega[i]**2 + (omega0**2 - omega[i]**2)**2)
    Xv[i] += Av[i]*np.sin(omega[i]*tv[i]) + Bv[i]*np.cos(omega[i]*tv[i])
    
    lineM.set_data(Xv[i],0)
    lineRes.set_data([-2,Xv[i]],[0,0])
    lineA.set_data(omega[i],np.sqrt(A[i]**2 + B[i]**2))
    
    return(lineX,lineM,lineRes,lineA,)

#Los plots fijos los hago por fuera de la función, no necesito que se grafiquen cada vez
m=1
F = 1
omega0 = 2
omega = np.linspace(0,5,500)
gamma = 0.5
A = (F/m)*gamma*omega/(gamma**2 * omega**2 + (omega0**2 - omega**2)**2)    
B = (F/m)*(omega0**2 - omega**2)/(gamma**2 * omega**2 + (omega0**2 - omega**2)**2)

axA.plot(omega,np.sqrt(A**2 + B**2),lw=2,color = 'k',ls = '--',zorder = 2)

#Creo la animación, le paso la figura donde graficarlos, la funcion animate que grafica las curvas en cada instancia, la función que inicializa, la cantidad de frames y el intervalo entre frame y frame. blit sirve para que grafique solamente los puntos que hayan cambiado en la animación, hace un poquito más rápido el proceso
anim = animation.FuncAnimation(fig,animate,init_func = init, frames = frames, interval = 50, blit = True)


#%% Guardo la animación
print('guardando imágen')
anim.save('resonancia.gif', writer='imagemagick')

#En win nunca pude instalar imageclick ni ningún otro writer que guarde en gif, uso esta parte que está comentada (sí hay que instalar ffmpeg), lo guardo en mp4 y después lo convierto con algún soft aparte.

#Writer = animation.writers['ffmpeg']
#writer = Writer(fps=20, metadata=dict(artist='Me'), bitrate=1800)

#anim.save('reso.mp4', writer=writer)
#fuck, tengo que instalar ffmpeg para usarlo esto


#%%
plt.show() #Tiene que ir despues de anim.save, sino tira un error que por ahora no pude solucionar
