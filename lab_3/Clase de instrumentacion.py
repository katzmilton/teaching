# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 20:37:30 2019

@author: Milt
"""

import numpy as np
import pyvisa as visa
import time
import matplotlib.pyplot as plt


#%% Me conecto con el generador y con el osciloscopio
#Creo la interfaz visa para comunicarme con equipos
rm = visa.ResourceManager()
#Pregunto que dispositvos están conectados
print(rm.list_resources())

rscosci = rm.list_resources('USB0::0x0699::0x0368::?*::INSTR')[0]#Version automatica par conectar
#rscfungen = rm.list_resources('USB::0x0699::0x0353::?*::INSTR')[0]
#Abro la comunicación con los equipos (Osc Tektronik TBS 1052B-EDU  +  Tektronik AFG1022)
osci = rm.open_resource(rscosci)
#fungen = rm.open_resource(rscfungen)

#%%Cambio la frecuencia y amplitud al generador

#defino los valores de las variables del generador de funciones
amplitude=5
offset=0.0
'''
Le mando la instrucción al equipo.
SOUR = Source, y el numero que le sigue es el canal que queremos usar
El %V lo que hace es "llamar" a un valor que esta afuera del comando.
En el caso de la frecuencia, el %f va a ser reemplazado por el numero 
cargado en la variable freq
'''
#fungen.write('SOUR1:FREQ 10MHZ')
fungen.write('SOUR1:VOLT %f' % amplitude) 
fungen.write('SOUR1:VOLT:OFFS %f' % offset)
fungen.write('OUTP1:STAT ON')

#%% Seteo algunas cosas que son necesarias para el osciloscopio
#Le pregunto al osciloscopio todas sus escalas
xze, xin, yze, ymu, yoff = osci.query_ascii_values('WFMPRE:XZE?;XIN?;CH1:YZE?;YMU?;YOFF?;', separator=';')
print(osci.query('WFMPRE:XZE?'))
#XZE=Xzero
#XIN=Intervalo de sampleo en el tiempo (eje x)
#YZE=Yzero
#YMU = Escala vertical
#YOF = Posición vertical
#Si los quiero independientemente a todos les tengo que anteceder "WFMPRE:", al pedirlos todos juntos puedo ponerlo solo una vez

#Eligo el modo de transmisión de datos (binario)
osci.write('DAT:ENC RPB')
osci.write('DAT:WID 1')
'''
The range is 0 to 255 when DATa:WIDth is 1. Center screen is 127. The range is
0 to 65,535 when DATa:WIDth is 2. The upper limit is one division above the top
of the screen and the lower limit is one division below the bottom of the screen.
'''

#%% Tomo datos del osciloscopio
#Con esto setteamos la alutra del trigger en la pantalla
osci.write('TRIG:MAI:LEV 0.1')

# Adquiere los datos del canal 1 y los devuelve en un array de numpy
data = osci.query_binary_values('CURV?', datatype='B', container=np.array)

#Ahora lo que adquirimos tiene que tomar los valores correspondientes
tiempo = xze + np.arange(len(data)) * xin

Volt = (data-yoff) * ymu + yze #yze es cúanto voltaje representa el punto más bajo de la pantalla (ya está en volts), mientras que el yoff es cuántos bits arriba del punto más bajo está el 0 volts? 

plt.plot(tiempo, Volt)

#%% barrido de frecuencias en generador
data = np.array([])
freq = np.logspace(0,6,10) #Hace un array logaritmicamente espaciado entre 10^0 y 10^6

#Es necesario ir cambiando la escala temporal (osci.write('horizontal:main:scale %f'))

for i in range(len(freq)):
    fungen.write('SOUR1:FREQ %f' % freq[i])
    time.sleep(1)
    data = np.append(data,osci.query_binary_values('CURV?', datatype='B', container=np.array))
