"""
Created on Mon Sep 23 23:19:38 2019

@author: Milt
"""
#%%
from scipy.io import wavfile as wav
import matplotlib.pyplot as plt
import numpy as np

#%%
#Cargo las señales grabadas, fs es la frecuencia de sampleo
fsg, globo = wav.read('Globo 1.3.wav')
fsv, voz = wav.read('mi voz.wav')

#Uso mi frecuencia de muestreo para definir un vector tiempo (En realidad tengo la misma frecuencia de muestreo para ambas señales, si no fuera así debería inventar valores en la mitad de la señal con menos fs para que queden las dos sincronizadas en el tiempo)
tg = np.arange(0,len(globo)/fsg,1/fsg)
tv = np.arange(0,len(voz)/fsv,1/fsv)


size = max(len(globo), len(voz))
# fsize = 2**np.ceil(np.log2(size)).astype(int)

h = np.fft.rfft(globo,size)
fvoz = np.fft.rfft(voz,size)

conv = h*fvoz



vozin = np.fft.irfft(conv)

vozin = vozin.real/max(vozin.real)
wav.write('vozin.wav',fsg, vozin)

# plt.plot(vozin.real)
# plt.plot(voz)
# plt.plot(globo)
