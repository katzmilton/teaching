# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 09:13:27 2019

@author: Milt
"""
#%%
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
#%%
t = np.arange(0,10,0.1)
med = np.exp(-t)
pval = np.array([])
norm01 = np.random.randn(1000)
sca = np.linspace(0.03,0.5,10)
j=0

for u in med:
    j+=1
    i=0
    for sig in sca:
        i += 1
        #        sig = 0.03*u
        norm = sig*norm01 + u
        logn = np.log(norm)
        KS,p = st.kstest(logn,'norm',args=(np.log(u),sig/u))
        if p<0.05:
            print(sig/u)
    

#plt.plot(t,med)
#plt.plot(pval,'ro')
#%%
mu = np.log(u)
des = sig/u
x = np.linspace(st.norm.ppf(0.001,loc=mu,scale=des),st.norm.ppf(0.999,loc=mu,scale=des), 100)
plt.plot(x, st.norm.pdf(x,loc=mu,scale=des))
plt.hist(logn,bins = 100,density = True)
#%%
u=np.log(10)
sig = 1/u
x = np.linspace(u-3*sig,u+3*sig,1000)
N = ((1/np.sqrt(2*np.pi*sig**2))*np.exp(-((x-u)/sig)**2))

plt.plot(x,N)