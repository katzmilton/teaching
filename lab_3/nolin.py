# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 08:29:37 2019

@author: Milt
"""

#%%
import scipy.optimize as opt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as op
#%%
noms = ['100 agua 30 Hz.txt','100 agua 15 Hz.txt','100 agua 10 Hz.txt','30 agua 15 Hz.txt','30 agua 30 Hz.txt','30agua 10 Hz.txt', '70 agua 10 Hz.txt','70 agua 15 Hz.txt','70 agua 30 Hz.txt']
for i in range(len(noms)):
        
    datos = pd.read_csv(noms[i], header = 2, delimiter = '\t')
    for j in range(len(datos)):
        datos['(N)'][j] = datos['(N)'][j].replace(",",".")
        datos['time'][j] = datos['time'][j].replace(",",".")
        
    datos = datos.astype(float)
    plt.figure(i)
    plt.title(noms[i])
    plt.plot(datos.time, datos['(N)'])

#%%

datos = pd.read_csv('70 agua 30 Hz.txt', header = 2, delimiter = '\t')

for i in range(len(datos)):
    datos['(N)'][i] = datos['(N)'][i].replace(",",".")
    datos['time'][i] = datos['time'][i].replace(",",".")
    

datos = datos[datos.time.astype(float) >= 2.8]
datos = datos.astype(float)
plt.plot(datos.time,datos['(N)'])

#%%
def modelo(t,lam , A, w, f, of):
    return A*np.exp(-lam*t)*np.cos(w*t+f) + of

# Parámetros iniciales con los que vamos a iniciar el proceso de fiteo
parametros_iniciales=[ 1.2, 6, 1.5, 7.5]

# Hacemos el ajuste con curve_fit
popt, pcov = op.curve_fit(modelo, datos.time, datos['(N)'], p0=parametros_iniciales)

# curve_fit devuelve dos resultados. El primero (popt) son los
# parámetros óptimos hallados. El segundo (pcov) es la matriz de
# covarianza de los parámetros hallados.

x_modelo  = np.linspace(datos.loc[84,'time'], datos.loc[300,'time'], 1000)

plt.figure()
plt.plot( datos.time, datos['(N)'],  'o', label='datos')
plt.plot(x_modelo, modelo(x_modelo, *popt), 'r-', label='modelo ajustado')
plt.legend(loc='best')
plt.xlabel('time')
plt.ylabel('y')
plt.tight_layout()