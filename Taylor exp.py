# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 14:20:28 2018

@author: Milton
"""
#%%
import numpy as np
import matplotlib.pyplot as plt
import math

from matplotlib import animation, rc
# from IPython import HTML, Image

#%%
rc('animation', html = 'html5') #no se que hace esto

#seteo la figura donde voy a graficar la animación
fig, ax = plt.subplots()
ax.set_xlim((-10,3))
ax.set_ylim((-0.1,3))
#num = ax.annotate(str(1), xy = (-8,2), xytext =(-8,2))

line, = ax.plot([], [], lw=2)

#Inicializo la función, ploteo el background de cada cuadro
def init():
    line.set_data([],[])
    return (line,)

#Función de animación. Se la llama secuencialmente
def animate(i):
    x = np.linspace(-10,3,1000)
    y = np.zeros((20,len(x)))
    for n in range(i+1):
        y[i] = y[i] + x**n/math.factorial(n)
    line.set_data(x,y[i])
    return(line,)

#Llamo al animador. blit = True que solo redibuje cosas que hayan cambiado
anim = animation.FuncAnimation(fig,animate,init_func = init, frames = 20, interval = 500, blit = True)
plt.plot(np.linspace(-10,3,1000),np.exp(np.linspace(-10,3,1000)))
plt.show()
#%%
# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

print('guardando imágen')
anim.save('taylor.gif', writer='imagemagick')
